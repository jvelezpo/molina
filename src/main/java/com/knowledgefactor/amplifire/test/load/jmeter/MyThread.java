package com.knowledgefactor.amplifire.test.load.jmeter;

import java.util.concurrent.CountDownLatch;

import com.knowledgefactor.amplifire.test.load.jmeter.AnswersHandlerFactory.AnswersHandler;

public class MyThread extends Thread {

	private final CountDownLatch stopLatch;
	private int i;

	public MyThread(CountDownLatch stopLatch, int i) {
		this.stopLatch = stopLatch;
		this.i = i;
	}

	public void run() {
		try {

			String questionId = "0921ace8-a614-46ba-a2a6-eefda20db48e";
			boolean useCorrectAnswers = false;
			String answerId = "";

			String a = "51090,51089,51088,51087,51058,51059,51060,11925,11924,11923,11922,51069,51068,51070,51100,51098,51099,294129,294126,294128,294127,51052,51051,51053,51064,51063,51061,ba83398f-2f31-48eb-8c1a-57a8065865da";
			String b = "1164697083,1164697085,1164697088,1164697086,1164697087,1164697088,1164697089,1164697090,1164697091,1164697092,1164697093,1164697094,1164697095,1164697096,1164697097,1164697098,1164697099,1164697100,1164697101,1164697102,1164697103,1164697104,1164697105,1164697106,1164697107,1164697108,1164697109,here is the correct one";

			AnswersHandlerFactory.getInstance(
					"/home/sebastian/Desktop/TEMP/molina/my-app/resources/answer_key_ConstitutionalLaw_ExecutivePower.txt");

			// answerId = AnswersHandler.getAnswer(questionId, useCorrectAnswers);
			answerId = AnswersHandler.getAnswer(questionId, useCorrectAnswers, a, b);

			System.out.println(i + " emora " + answerId);

		} finally {
			stopLatch.countDown();
		}
	}
}