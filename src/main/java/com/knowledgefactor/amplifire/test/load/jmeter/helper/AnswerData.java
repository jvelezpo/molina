package com.knowledgefactor.amplifire.test.load.jmeter.helper;

/**
 * This models the relevant data to extract from a CSV line and populate the
 * data map that will be held on memory to process the answers i na convenient
 * manner.
 *
 * @author ricardo.buitrago@globant.com
 *
 */
public class AnswerData {

    /**
     * Constant integer holding the number of tokens per CSV line that represents
     * an answer
     */
    private final static int CSV_LINE_TOKENS = 3;

    /**
     * The key for the answer as stored in the current DB
     */
    private String answerKey;

    /**
     * A flag that tells whether this answer is linked to the questions as the
     * correct answer
     */
    private boolean correctAnswer;
    /**
     * The content of the answer which can be either a plain String or a given set
     * of multimedia types (unused)
     */
    private Object answerContent;

    /**
     * a reference to the question that is being assessed by the sampler
     */
    private String questionKey;

    /**
     * Process every line of the file and load it into memory
     * @param csvLine
     */
    public AnswerData(final String csvLine) {

        String[] splitString = csvLine.split("\\|");
        if (splitString.length < CSV_LINE_TOKENS) {
            throw new IllegalArgumentException("Fatal: Expected at least " + CSV_LINE_TOKENS + " tokens, but got " + splitString.length);
        }

        // Set the values from the file into memory in their own setter methods
        setQuestionKey(splitString[0]);
        setCorrectAnswer(Boolean.valueOf(splitString[1]));
        setAnswerKey(splitString[2]);
    }

    public String getQuestionKey() {
        return questionKey;
    }

    public void setQuestionKey(String questionKey) {
        this.questionKey = questionKey;
    }

    public String getAnswerKey() {
        return answerKey;
    }

    public void setAnswerKey(String answerKey) {
        this.answerKey = answerKey;
    }

    public boolean isCorrectAnswer() {
        return correctAnswer;
    }

    public void setCorrectAnswer(boolean correctAnswer) {
        this.correctAnswer = correctAnswer;
    }

    public Object getAnswerContent() {
        return answerContent;
    }

    public void setAnswerContent(Object answerContent) {
        this.answerContent = answerContent;
    }

}
