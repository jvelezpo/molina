package com.knowledgefactor.amplifire.test.load.jmeter;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

import com.knowledgefactor.amplifire.test.load.jmeter.helper.QuestionData;
import com.knowledgefactor.amplifire.test.load.jmeter.io.CsvFileReader;

/**
 * This is a Factory class that handles all the possible instances of
 * AnswersHandler
 *
 * @author elmer.mora@globant.com, ricardo.buitrago@globaFnt.com
 */
public class AnswersHandlerFactory {

    private static final String DEFAULT_ANSWER = "XXXXXXXX-xxxx-xxxx-xxxx-xxxxxxxxxxxx";

    /**
     * Static class that does the invocation of the {@link CsvFileReader} class
     *
     * @author ricardo.buitrago@globant.com
     *
     */
    public static final class AnswersHandler {
        // The answers map
        private static Map<String, QuestionData> questionsMap;

        /**
         * Loads the file in memory and get it ready to be use
         * @param csvFilePath
         * @throws FileNotFoundException
         * @throws IOException
         */
        private AnswersHandler(final String csvFilePath) throws FileNotFoundException, IOException {

            // Sets the path to the file with the answers
            CsvFileReader reader = new CsvFileReader(csvFilePath);

            // Loads the file in memory
            reader.readCsvFile();

            // Set the answers from the file in memory
            questionsMap = reader.getQuestionMap();
        }

        /**
         * Obtain an answer base on the 'correct' flag from the question id pass in the first parameter
         * @param questionKey id of the question to obtain
         * @param correct flag that indicates which answer to return, a true one or a false one
         * @return
         */
        public static String getAnswer(final String questionKey, boolean correct) {
            QuestionData question = null;

            // Queries the question with the id that came as parameter
            question = questionsMap.get(questionKey);

            // return the answer in memory base in the parameter 'correct'
            return question == null ? DEFAULT_ANSWER : question.getAnswer(correct);
        }

        /**
         * Obtain an answer base on the 'correct' flag from the question id pass in the first parameter
         * And does a matching with the id and return the long one
         * @param questionKey id of the question to obtain
         * @param correct flag that indicates which answer to return, a true one or a false one
         * @param answers list separate by ',' containing short answers id
         * @param ids list separate by ',' containing long answers id
         * @return
         */
        public static String getAnswer(final String questionKey, boolean correct, String answers, String ids) {
            String answerId = getAnswer(questionKey, correct);

            String[] a = answers.split(",");
            String[] i = ids.split(",");

            int index = Arrays.asList(a).indexOf(answerId);

            if (index >= 0) {
                return i[index];
            } else {
                return "ID NOT FOUND";
            }
        }
    }

    /**
     * The Map that stores all references to {@link AnswersHandler}
     */
    private static Map<String, AnswersHandler> factoryStore = Collections.synchronizedMap(new HashMap<String, AnswersHandler>());

    /**
     * Creates a single instance per file to be use as answers file processor
     * @param someCsvFilePath this is the path to the file with the answers
     * @return the instance with the data inside the file loaded and ready to be query
     */
    public static AnswersHandler getInstance(final String someCsvFilePath) {
        synchronized (factoryStore) {
            AnswersHandler instance = factoryStore.get(someCsvFilePath);
            if (instance == null) {
                try {
                    instance = new AnswersHandler(someCsvFilePath);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                factoryStore.put(someCsvFilePath, instance);
            }
            return instance;
        }

    }

    /**
     * An old fashioned test
     * @param args
     * @throws InterruptedException
     */
    public static void main(String args[]) throws InterruptedException {

        long startTime = System.currentTimeMillis();

        CountDownLatch cdl = new CountDownLatch(10000);
        for (int i = 0; i < 10000; i++) {
            MyThread thread = new MyThread(cdl, i);
            thread.start();
        }
        cdl.await();

        long stopTime = System.currentTimeMillis();
        long elapsedTime = stopTime - startTime;
        System.out.println(elapsedTime);
    }
}
