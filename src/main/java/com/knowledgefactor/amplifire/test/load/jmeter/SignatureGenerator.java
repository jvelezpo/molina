package com.knowledgefactor.amplifire.test.load.jmeter;


import java.io.ByteArrayOutputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.SecureRandom;
import java.security.Signature;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;

import org.apache.commons.codec.binary.Base64;

public class SignatureGenerator {
	
	public static String getRequestSignature(String requestData, String privateKey) {
		
		byte[] bytes = getSignature(requestData, privateKey);
		String signature = new String(Base64.encodeBase64(bytes));
		
		return signature;
	}
	
	private static byte[] getSignature(Serializable object, String privateKey)
	{
	try
		{
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		ObjectOutput out = new ObjectOutputStream(bos);
		out.writeObject(object);
		Signature signer = Signature.getInstance("DSA");
		signer.initSign(getPrivateKey(privateKey), new SecureRandom());
		signer.update(bos.toByteArray());
		return signer.sign();
		
		} catch (Exception e) {
		throw new RuntimeException(e.toString());
		}
	}
	
	private static PrivateKey getPrivateKey(String privateKey) throws NoSuchAlgorithmException, InvalidKeySpecException {
		PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(Base64.decodeBase64(privateKey.getBytes()));
		KeyFactory factory = KeyFactory.getInstance("DSA");
	
		return factory.generatePrivate(spec);
	} 
}
