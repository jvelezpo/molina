package com.knowledgefactor.amplifire.test.load.jmeter;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Map;

import com.knowledgefactor.amplifire.test.load.jmeter.helper.QuestionData;
import com.knowledgefactor.amplifire.test.load.jmeter.io.CsvFileReader;

/**
 * @author elmer.mora@globant.com, ricardo.buitrago@globant.com
 */
public class AnswersRepository {

  // Implements as a singleton, so a unique instance exists
  private static AnswersRepository answersRepInstance = null;

  // The answers map
  private static Map<String, QuestionData> questionsMap;

  private AnswersRepository() throws FileNotFoundException, IOException {
    CsvFileReader reader = new CsvFileReader(
                                             "src/test/resources/answer_key_ConstitutionalLaw_ExecutivePower.txt");
    reader.readCsvFile();
    questionsMap = reader.getQuestionMap();
  }

  public static AnswersRepository getInstance() {
    if (answersRepInstance == null) {
      try {
        answersRepInstance = new AnswersRepository();
      } catch (FileNotFoundException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      } catch (IOException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
    }

    AnswersRepository repo = null;
    try {
      repo = new AnswersRepository();
    } catch (FileNotFoundException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

    return repo;
  }

  public static String getAnswer(final String questionKey, boolean correct) {
    return questionsMap.get(questionKey).getAnswer(correct);
  }

}
