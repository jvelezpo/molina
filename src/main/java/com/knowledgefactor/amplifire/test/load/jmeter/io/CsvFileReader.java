package com.knowledgefactor.amplifire.test.load.jmeter.io;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

import com.knowledgefactor.amplifire.test.load.jmeter.helper.AnswerData;
import com.knowledgefactor.amplifire.test.load.jmeter.helper.QuestionData;

public class CsvFileReader {

    public final static String LNSEPARATOR = System.getProperty("line.separator");

    private String path = null;

    private StringBuilder buffer = new StringBuilder();

    private HashMap<String, QuestionData> questionMap = new HashMap<String, QuestionData>();

    /**
     * Sets the path to the file with the answers
     * @param thePath
     */
    public CsvFileReader(final String thePath) {
        path = thePath;
    }

    /**
     * Loads the file in memory
     * @throws FileNotFoundException
     * @throws IOException
     */
    public void readCsvFile() throws FileNotFoundException, IOException {
        BufferedReader bufrd = new BufferedReader(new FileReader(path));
        String line = "";
        // Read line by line
        while ((line = bufrd.readLine()) != null) {
            buffer.append(line).append(LNSEPARATOR);
            // Process the line from the file
            AnswerData answerEntry = new AnswerData(line);

            // Load the id from the line into memory
            QuestionData question = questionMap.get(answerEntry.getQuestionKey());
            if (question == null) {
                question = new QuestionData();
                question.setQuestionKey(answerEntry.getQuestionKey());
                questionMap.put(answerEntry.getQuestionKey(), question);
            }

            // Set the correct answer in its own setter method using it value
            if (answerEntry.isCorrectAnswer()) {
                question.setCorrectAnswerKey(answerEntry.getAnswerKey());
            } else {
                question.getIncorrectAnswersKeys().add(answerEntry.getAnswerKey());
            }

            // Load the AnswerKey from the line into memory
            question.getAnswerMap().put(answerEntry.getAnswerKey(), answerEntry);
        }
        bufrd.close();
    }

    /**
     * Getters && Setters
     */

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public StringBuilder getBuffer() {
        return buffer;
    }

    public void setBuffer(StringBuilder buffer) {
        this.buffer = buffer;
    }

    public HashMap<String, QuestionData> getQuestionMap() {
        return questionMap;
    }

    public void setQuestionMap(HashMap<String, QuestionData> questionMap) {
        this.questionMap = questionMap;
    }

}
