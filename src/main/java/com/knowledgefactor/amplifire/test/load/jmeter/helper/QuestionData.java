package com.knowledgefactor.amplifire.test.load.jmeter.helper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author ricardo.buitrago@globant.com
 */
public class QuestionData {

    private String questionKey;

    private String correctAnswerKey;

    private List<String> incorrectAnswersKeys = new ArrayList<String>();

    private Map<String, AnswerData> answerMap = new HashMap<String, AnswerData>();

    /**
     * Getters && Setters
     */

    public String getQuestionKey() {
        return questionKey;
    }

    public void setQuestionKey(String questionKey) {
        this.questionKey = questionKey;
    }

    public String getCorrectAnswerKey() {
        return correctAnswerKey;
    }

    public void setCorrectAnswerKey(String correctAnswerKey) {
        this.correctAnswerKey = correctAnswerKey;
    }

    public List<String> getIncorrectAnswersKeys() {
        return incorrectAnswersKeys;
    }

    public void setIncorrectAnswersKeys(List<String> incorrectAnswersKeys) {
        this.incorrectAnswersKeys = incorrectAnswersKeys;
    }

    public Map<String, AnswerData> getAnswerMap() {
        return answerMap;
    }

    public void setAnswerMap(Map<String, AnswerData> answerMap) {
        this.answerMap = answerMap;
    }

    public String getAnswer(final boolean correct) {
        if (correct) {
            return getCorrectAnswerKey();
        }
        else {
            return getIncorrectAnswersKeys().get(0);
        }
    }

}
