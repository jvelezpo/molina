package com.knowledgefactor.amplifire.test.load.jmeter.uploader;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyWriter;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.multipart.BodyPart;
import com.sun.jersey.multipart.FormDataBodyPart;
import com.sun.jersey.multipart.FormDataMultiPart;
import com.sun.jersey.multipart.file.FileDataBodyPart;

/**
 * This simple Jersey client consumes ImportJtlService
 * 
 * @author ricardo.buitrago@globant.com
 */
public class ImportWsClient {

  private static Logger logger = Logger.getLogger(ImportWsClient.class);
  
  private FormDataMultiPart form = new FormDataMultiPart();
  
  private ClientResponse clientResponse;

  public ImportWsClient() {
     BasicConfigurator.configure();
  }


  /**
   * @param url RWS active URL
   * @param filePath Full path to the file in the local machine
   * @param executionName Name for the execution. JMeter integration will use execution variables such as timestamp ({@link SimpleDateFormat} yyyyMMdd-HHmmss), followed by thread count. When used in integrations other than JMeter, it is recommended to use a timestamp and threadcount, at least.
   * @param executionHost Name or IP address of the host, depending on which is more descriptive.
   * @param testName The name of the test. JMeter integration will use JMX name for this, which is otherwise enough to describe the scenario being tested.
   * @param versionId The Id for the Current version being tested. It will match a version Id from the DB in the following way: 
    <pre>
 id |  name  
----+--------
  1 | v6.2.7
  2 | v6.3.0
  3 | v6.3.1
  4 | v6.3.2
  5 | v6.3.3
   </pre>. If a value is specified in "versionName" argument, a new version will be created on the DB table.
   * @param versionName Contains a value only when creating a new version. 
   * @param testTypeId The id for the current test type. It will match a test type Id from the DB in the following way:
<pre>
 id |    type     
----+-------------
  1 | performance
  2 | distributed
  3 | maintenance
  4 | endurance
  5 | sso
  6 | jenkins   
</pre>. If a value is specified in "testTypeName" argument, a new test type will be created on the DB table.
   * @param testTypeName Contains a value only when creating a new version.
   * @return a Client response.
   */
  public ClientResponse upload(final String url, final String filePath,
      final String executionName, final String executionHost,
      final String testName, final String versionId, final String versionName,
      final String testTypeId, final String testTypeName) {

    String fixedTestName = handleParams(testName)[0];
    
    try {
      File f = new File(filePath);

      logger.debug("File " + filePath + " found? " + f.exists());

      WebResource webResource = Client.create().resource(url);
      
      logger.debug("web resource created? " + webResource.getURI());

      form.bodyPart(new FileDataBodyPart("file", f,
	  MediaType.MULTIPART_FORM_DATA_TYPE));
      
      form.bodyPart(new FormDataBodyPart("executionName", executionName))
	  .bodyPart(new FormDataBodyPart("executionHost", executionHost))
	  .bodyPart(new FormDataBodyPart("testName", fixedTestName))
	  .bodyPart(new FormDataBodyPart("versionId", versionId))
	  .bodyPart(new FormDataBodyPart("versionName", versionName))
	  .bodyPart(new FormDataBodyPart("testTypeId", testTypeId))
	  .bodyPart(new FormDataBodyPart("testTypeName", testTypeName));

      if (logger.isDebugEnabled()) {
	for (BodyPart part : form.getBodyParts()) {
	  logger.debug("\tCreated "
	      + new FormDataBodyPartStringWrapper((FormDataBodyPart) part));
	}
      }

      clientResponse = webResource.type(MediaType.MULTIPART_FORM_DATA).post(
	  ClientResponse.class, form);

      logger.info("Successful POST? The response was .. "
	  + clientResponse.getStatus());

      System.out.println("Client response status code: "
	  + clientResponse.getClientResponseStatus());
    } catch (IllegalArgumentException e) {
      logger.error("POST failed due to an argument error! ", e);
    } catch (UniformInterfaceException e) {
      logger.error("POST failed due to URI error! ", e);
    } catch (ClientHandlerException e) {
      logger.error("POST failed due to handler exception", e);
    }

    return clientResponse;
  }
  
//  @Provider
  class LongType_MessageBodyWriter implements MessageBodyWriter<Long> {

    public boolean isWriteable(Class<?> type, Type genericType,
	Annotation[] annotations, MediaType mediaType) {
      return type.isAssignableFrom(Long.class) && mediaType == MediaType.TEXT_PLAIN_TYPE;
    }

    public long getSize(Long t, Class<?> type, Type genericType,
	Annotation[] annotations, MediaType mediaType) {
      return -1;
    }

    public void writeTo(Long t, Class<?> type, Type genericType,
	Annotation[] annotations, MediaType mediaType,
	MultivaluedMap<String, Object> httpHeaders, OutputStream entityStream)
	throws IOException, WebApplicationException {
      final Writer writer = new BufferedWriter(new OutputStreamWriter(entityStream)); // FIXME - charset???
      writer.write(Long.toString(t));
      writer.flush();
    }
    
  }
  
  private String[] handleParams(String ... params) {
    List<String> fixedParams = new ArrayList<String>();
    for (String param: params) {
      if (param.contains(".jmx")) {
	param = param.replaceAll("\\.jmx", "");	
      }
      fixedParams.add(param);
    }
    return fixedParams.toArray(new String[params.length]);
  }

  public static void main(String... args) {
    String url = "http://10.130.10.113:8080/ReportWS/KF/import/jtl";
    String fileName = "src/test/resources/20130219-131053-1U.csv";
    String executionName = "20130219-131053-1U";
    String testName = "jmetersupport-maintenance";
    String executionHost = "localhost";
    String versionId = "1L";
    String testTypeId = "1L";
    new ImportWsClient().upload(url, fileName, executionName,
	executionHost, testName, versionId, "", testTypeId, "");
  }

  class FormDataBodyPartStringWrapper {
    private String name;
    private Object entity;
    private MediaType type;

    public FormDataBodyPartStringWrapper(FormDataBodyPart part) {
      name = part.getName();
      entity = part.getEntity();
      type = part.getMediaType();
    }

    @Override
    public String toString() {
      return "FormDataBodyPartStringWrapper [name=" + name + ", entity="
	  + entity + ", type=" + type + "]";
    }

  }

  public FormDataMultiPart getForm() {
    return form;
  }

  public void setForm(FormDataMultiPart form) {
    this.form = form;
  }

  public ClientResponse getClientResponse() {
    return clientResponse;
  }

  public void setClientResponse(ClientResponse clientResponse) {
    this.clientResponse = clientResponse;
  }
}