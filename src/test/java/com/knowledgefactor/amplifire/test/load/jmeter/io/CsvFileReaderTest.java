package com.knowledgefactor.amplifire.test.load.jmeter.io;

import static org.junit.Assert.*;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

public class CsvFileReaderTest {

  private final static String PATH = "/home/sebastian/Desktop/TEMP/molina/my-app/resources/answer_key_ConstitutionalLaw_ExecutivePower.txt";
  
  private final static int LINES = 117;
  
  private final static int QUESTIONS = 32;

  private CsvFileReader underTest;

  @Before
  public void setUp() {
    underTest = new CsvFileReader(PATH);
  }

  @Test
  public void testReadCsvFile() throws FileNotFoundException, IOException {
    underTest.readCsvFile();
    assertTrue(underTest.getBuffer().length() > 0);
  }

  @Test
  public void testGetPath() {
    assertEquals(PATH, underTest.getPath());
  }

  @Test
  public void testGetBuffer() throws FileNotFoundException, IOException {
    underTest.readCsvFile();
    String[] splitLines = underTest.getBuffer().toString().split(CsvFileReader.LNSEPARATOR);
    int lines = splitLines.length;
    assertEquals(LINES, lines);
  }

  @Test
  public void testGetQuestionMap() throws FileNotFoundException, IOException {
    underTest.readCsvFile();
    int items = underTest.getQuestionMap().keySet().size();
    assertEquals(QUESTIONS, items);
  }

}
