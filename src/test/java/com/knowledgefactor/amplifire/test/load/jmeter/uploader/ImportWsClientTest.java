package com.knowledgefactor.amplifire.test.load.jmeter.uploader;

import static org.junit.Assert.*;

import java.io.File;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.mockito.Matchers.*;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.ClientResponse.Status;
import com.sun.jersey.multipart.BodyPart;
import com.sun.jersey.multipart.FormDataBodyPart;
import com.sun.jersey.multipart.FormDataMultiPart;
import com.sun.jersey.multipart.file.FileDataBodyPart;

public class ImportWsClientTest {

  private String url;
  private String fileName;
  private String executionName;
  private String executionHost;
  private String testName;

  private ImportWsClient underTest;

  private Client mockClient;

  private WebResource mockWebResource;

  private FormDataMultiPart mockFormDataMultipart;

  private FileDataBodyPart mockFileDataBodyPart;

  private ClientResponse mockClientResponse;

  private File mockFile;

  private List<BodyPart> mockBodyParts;

  @Before
  public void setUp() {
    url = "http://192.168.0.14:8080/ReportWS/KF/Learning/import/jtl";
    fileName = "src/test/resources/20130219-131053-1U.csv";
    executionName = fileName.substring(fileName.lastIndexOf("/") + 1);
    executionName = executionName.substring(0, executionName.length() - 4)
	+ "_" + new java.util.Date().getTime();
    
    try {
      executionHost = InetAddress.getLocalHost().getHostName();
    } catch (UnknownHostException e) {
      executionHost = "localhost";
    }
    
    testName = "ImportWsClientTest";
    
    underTest = new ImportWsClient();

    mockClientResponse = mock(ClientResponse.class);

    mockFormDataMultipart = mock(FormDataMultiPart.class);

    mockFileDataBodyPart = mock(FileDataBodyPart.class);

    mockWebResource = mock(WebResource.class);

    mockClient = mock(Client.class);

    mockFile = mock(File.class);

    mockBodyParts = mock(List.class);
  }

  @Test  
  public void testUpload() {
    when(mockClientResponse.getStatus()).thenReturn(Status.OK.getStatusCode());
    underTest.setClientResponse(mockClientResponse);
    
    when(mockFile.getName()).thenReturn(fileName);
    when(mockFileDataBodyPart.getFileEntity()).thenReturn(mockFile);
    when(mockFormDataMultipart.bodyPart(mockFileDataBodyPart)).thenReturn(mockFormDataMultipart);
    
    FormDataBodyPart part1 = mock(FormDataBodyPart.class);
    when(part1.getName()).thenReturn("executionName");
    when(part1.getValue()).thenReturn(executionName);
    when(mockFormDataMultipart.bodyPart(eq(part1))).thenReturn(mockFormDataMultipart);
    
    FormDataBodyPart part2 = mock(FormDataBodyPart.class);
    when(part2.getName()).thenReturn("executionHost");
    when(part2.getValue()).thenReturn(executionHost);
    when(mockFormDataMultipart.bodyPart(part2)).thenReturn(mockFormDataMultipart);
    
    FormDataBodyPart part3 = mock(FormDataBodyPart.class);
    when(part3.getName()).thenReturn("testName");
    when(part3.getValue()).thenReturn(testName);
    when(mockFormDataMultipart.bodyPart(part3)).thenReturn(mockFormDataMultipart);
    
    underTest.setForm(mockFormDataMultipart);
    
    when(mockWebResource.post((ClientResponse.class), mockFormDataMultipart)).thenReturn(mockClientResponse);
    
    //TODO finish this
    
    assert(true);
    
//    ClientResponse clientResponse = underTest.upload(url, fileName, executionName, executionHost, testName);
//    verify(mockWebResource).post(ClientResponse.class, mockFormDataMultipart);
  }
  
  
  public void integrationTest() {
    when(mockFile.getName()).thenReturn(fileName);
    when(mockFileDataBodyPart.getFileEntity()).thenReturn(mockFile);
    when(mockFileDataBodyPart.getName()).thenReturn("file.ext");
    when(mockBodyParts.size()).thenReturn(1);
    when(mockBodyParts.get(eq(0))).thenReturn(mockFileDataBodyPart);
    when(mockFormDataMultipart.getBodyParts()).thenReturn(mockBodyParts);
    when(mockClientResponse.getStatus()).thenReturn(Status.OK.getStatusCode());
    when(mockWebResource.post((ClientResponse.class), mockFormDataMultipart)).thenReturn(mockClientResponse);
    ClientResponse clientResponse = underTest.upload(url, fileName, executionName, executionHost, testName, "", "", "", "");
    assertEquals(mockClientResponse.getStatus(), clientResponse.getStatus());
  }

}
