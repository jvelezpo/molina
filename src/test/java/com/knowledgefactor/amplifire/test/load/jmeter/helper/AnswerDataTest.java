package com.knowledgefactor.amplifire.test.load.jmeter.helper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class AnswerDataTest {

	private AnswerData underTest;

	private final static String QUESTION_KEY = "0921ace8-a614-46ba-a2a6-eefda20db48e";
	private final static String CORRECT = "true";
	private final static String ANSWER_KEY = "4459dd0c-da1a-46ee-bbab-a01539b0f8e1";
	private final static String ANSWER_TEST_RC = "Executive agreements may not conflict with existing federal statutes.";
	private final static String CSV_LINE = new StringBuilder(QUESTION_KEY)
			.append("|").append(CORRECT).append("|").append(ANSWER_KEY)
			.append("|").append(ANSWER_TEST_RC).toString();

	@Before
	public void setUp() {
		underTest = new AnswerData(CSV_LINE);
	}

	@Test
	public void testGetQuestionKey() {
		assertEquals(QUESTION_KEY, underTest.getQuestionKey());
	}

	@Test
	public void testGetAnswerKey() {
		assertEquals(ANSWER_KEY, underTest.getAnswerKey());
	}

	@Test
	public void testIsCorrectAnswer() {
		assertTrue(Boolean.valueOf(CORRECT) == underTest.isCorrectAnswer());
	}

	@Test
	public void testGetAnswerContent() {
		assertNull(underTest.getAnswerContent());
	}

}
