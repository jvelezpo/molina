Questions and answers
================

##Purpose

Load a file answers into memory with short Id and all 4 answers.
Process it and return the long id of the answer base on what is requiere, the true answer or a incorrect one

###How to use

First get the instance from the jar passing the path to the file to use, like this:
```
com.knowledgefactor.amplifire.test.load.jmeter.AnswersHandlerFactory$AnswersHandler rep = com.knowledgefactor.amplifire.test.load.jmeter.AnswersHandlerFactory.getInstance("<path-to-file>");
```
Get the answer like this
```
answerId = rep.getAnswer(<questionId>, <boolean-what-is-needed>, <shor-id-list>, <long-id-list>);
```

###Output

The output will be the long id of the answer base on the value pass on the second parameter, if this parameter is true the 'answerId' will be the correct one, if the parameter is false then the 'answerId' will be a incorrect answer.

###Author
Author name / Author email:

juan sebastian velez / juan.velez@globant.com